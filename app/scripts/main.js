// SMOOTH SCROLL
$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.not('[data-toggle="tab"]')
.click(function(event) {
// On-page links
if (
location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
&&
location.hostname == this.hostname
) {
// Figure out element to scroll to
var target = $(this.hash);
target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
// Does a scroll target exist?
if (target.length) {
// Only prevent default if animation is actually gonna happen
event.preventDefault();
$('html, body').animate({
  scrollTop: target.offset().top - 69
}, 1000, function() {
  // Callback after animation
  // Must change focus!
  /*var $target = $(target);
  $target.focus();
  if ($target.is(':focus')) { // Checking if the target was focused
    return false;
  } else {
    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
    $target.focus(); // Set focus again
  };*/
});
}
}
});

//HEADER ATIVO
$(window).on('load', function () {
  var $win = $(window);
  var winH = $win.height();
  $(window).scroll(function () {
      if ($(this).scrollTop() >= 10) {
          $('#header').addClass('ativo');
      } else {
          $('#header').removeClass('ativo');
      }
  });
  if ($(this).scrollTop() >= 150) {
      $('#header').addClass('ativo');
  }
});

if ($('.slider_presentation').length) {
  $('.slider_presentation').slick({
    autoplay: true,
    infinite: true,
    autoplaySpeed: 4000,
    dots: true,
    arrows: false,
    fade: false,
    cssEase: 'linear',
    responsive: [{
        breakpoint: 500,
        settings: {
            dots: true,
            arrows: false
        }
    }]
    });
  }

if ($('.slider_depoimentos').length) {
  $('.slider_depoimentos').slick({
    autoplay: true,
    infinite: true,
    autoplaySpeed: 4000,
    dots: true,
    arrows: false,
    fade: false,
    cssEase: 'linear',
    responsive: [{
        breakpoint: 500,
        settings: {
            dots: true,
            arrows: false
        }
    }]
    });
  }
